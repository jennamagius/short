fn main() {
    let stdin = std::io::stdin();
    let mut stdin_lock = stdin.lock();
    let stdout = std::io::stdout();
    let mut stdout_lock = stdout.lock();
    let mut line_buffer: Vec<u8> = Vec::new();
    let mut read_buffer = [0u8];
    let max_len: usize = match std::env::args().nth(1) {
        None => 200,
        Some(x) => match x.parse() {
            Ok(x) => x,
            Err(_err) => {
                eprintln!("Usage: short [length]");
                std::process::exit(1);
            }
        },
    };
    loop {
        match std::io::Read::read(&mut stdin_lock, &mut read_buffer) {
            Ok(1) => {
                if read_buffer[0] == b'\n' {
                    if line_buffer.len() <= max_len {
                        line_buffer.push(read_buffer[0]);
                        match std::io::Write::write_all(&mut stdout_lock, &line_buffer) {
                            Ok(_) => (),
                            Err(_err) => {
                                ();
                            }
                        }
                    }
                    line_buffer.clear();
                } else {
                    if line_buffer.len() <= max_len {
                        line_buffer.push(read_buffer[0]);
                    }
                }
            }
            Ok(0) => {
                break;
            }
            Ok(_) => {
                std::process::exit(2);
            }
            Err(err) => {
                panic!("{}", err);
            }
        }
    }
}
